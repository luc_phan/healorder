local L = LibStub("AceLocale-3.0"):NewLocale("HealOrder", "enUS", true)

L["Initializing..."] = true
L["Enabling..."] = true
L["Disabling..."] = true

L["Enabled"] = true

L["Healers"] = true
L["Name"] = true

L["Healer"] = true
L["Healer name"] = true
L["Add"] = true
L["Invalid healer name"] = true
L["Remove"] = true
L["Move up"] = true
L["Move down"] = true

L["Announce"] = true
L["Channel"] = true
L["None"] = true
L["Chat window"] = true
L["Say"] = true
L["Group"] = true
L["Raid"] = true
L["Warning"] = true
L["Test"] = true

L["Corrupted Mind"] = true
L["Forbearance"] = true
L["Weakened Soul"] = true

L["%s can still heal!"] = true
