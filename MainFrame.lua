-- ###########
-- # Globals #
-- ###########

local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local AceGUI = LibStub("AceGUI-3.0")

-- ##############
-- # Meta class #
-- ##############

MainFrame = {}

-- Base class method new

function MainFrame:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    setglobal(o.variable, nil) -- must be global, don't remember why

    return o
end

function MainFrame:Show()
    local mainFrame = self:CreateMainFrame()
    setglobal(self.variable, mainFrame)
    tinsert(UISpecialFrames, self.variable) -- allow ESC to close window
    AceConfigDialog:Open(self.addonName, mainFrame)
end

function MainFrame:CreateMainFrame()
    local MainFrame = AceGUI:Create("Frame")
    local title = self.addonName .. " " .. self.version
    MainFrame:SetTitle(title)
    MainFrame:SetStatusText(title)
    MainFrame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end) -- releases all widgets -_-
    MainFrame:SetLayout("Flow")
    MainFrame.frame:SetMovable(true)
    MainFrame.frame:SetScript(
        "OnMouseDown",
        function(button)
            MainFrame.frame:StartMoving()
            MainFrame.isMoving = true
        end
    )
    MainFrame.frame:SetScript(
        "OnMouseUp",
        function(button)
            if (MainFrame.isMoving) then
                MainFrame.isMoving = false

                local frame = MainFrame.frame
                frame:StopMovingOrSizing()

                local obj = frame.obj
                local status = obj.status or obj.localstatus
                status.width = frame:GetWidth()
                status.height = frame:GetHeight()
                status.top = frame:GetTop()
                status.left = frame:GetLeft()
            end
        end
    )
    return MainFrame
end

function MainFrame:IsVisible()
    return getglobal(self.variable) and getglobal(self.variable):IsVisible()
end

function MainFrame:Hide()
    getglobal(self.variable):Hide()
end
