-- #############
-- # Constants #
-- #############

local ADDON_NAME = 'HealOrder'

-- ###########
-- # Globals #
-- ###########

local AceGUI = LibStub("AceGUI-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale(ADDON_NAME)

-- ##############
-- # Meta class #
-- ##############

HealerTableWidget = CarTable:new()

function HealerTableWidget:new(o)
	o = o or CarTable:new(o)
	setmetatable(o, self)
	self.__index = self

	self.name = 'HealerTable'
	self.columns = {
		{header='name', caption=L["Name"], width=100}
	}
	self.rows = 8

	return o
end

-- ##########
-- # Widget #
-- ##########

do
    local widgetType = 'HealerTable'
    local widgetVersion = 1

    local function Constructor()
		local widget = {}
		widget.type = widgetType

		local table = HealerTableWidget:new(nil)
		widget.frame = table:BuildFrame()

        widget.OnAcquire = function(self)
            self:SetWidth(table:GetFrameWidth())
            self:SetHeight(table:GetFrameHeight())
		end

        widget.SetLabel = function(self, text) -- Set the text for the label.
        end

		widget.SetList = function(self, list) -- Set the list of values for the dropdown (key => value pairs)
			table:SetList(list)
			table:Update()
        end

		widget.SetValue = function(self, value) -- Set the value to an item in the List.
			table:SetSelected(value)
        end

		table.OnSelected = function(self, value)
			widget:Fire("OnValueChanged", value)
		end

		return AceGUI:RegisterAsWidget(widget)
	end

    AceGUI:RegisterWidgetType(widgetType, Constructor, widgetVersion)
end
