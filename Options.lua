-- #############
-- # Constants #
-- #############

local ADDON_NAME = "HealOrder"

-- ###########
-- # Globals #
-- ###########

local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale(ADDON_NAME)

-- ##############
-- # Meta class #
-- ##############

HealOrderOptions = {}

-- Base class method new

function HealOrderOptions:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    o.content = o:CreateContent()
    o.healerName = nil
    o.healerSelected = nil

    return o
end

function HealOrderOptions:CreateContent()
    local healerTable = HealerTableWidget:new()

    return {
        handler = self,
        type = "group",
        args = {
            enable = {
                order = 1,
                name = L["Enabled"],
                type = "toggle",
                set = "SetEnabled",
                get = "GetEnabled"
            },
            healersGroup = {
                order = 2,
                type = 'group',
                inline = true,
                name = L["Healers"],
                args = {
                    healersWidget = {
                        order = 1,
                        type = 'select',
                        name = L["Healers"],
                        values = self.db.healers,
                        dialogControl = "HealerTable",
                        width = healerTable:GetFrameWidth() / 170, -- width coefficient (multiplied by width_multiplier (= 170) in AceConfigDialog-3.0.lua)
                        get = function()
                            return self.healerSelected -- must be a key of values (see "AceConfigDialog-3.0.lua")
                        end,
                        set = function(info, value)
                            self.healerSelected = value
                            if value then
                                self.healerName = self.db.healers[value].name
                            else
                                self.healerName = nil
                            end
                        end
                    }
                }
            },
            healerGroup = {
                order = 3,
                type = 'group',
                inline = true,
                name = L["Healer"],
                args = {
                    newHealerName = {
                        order = 1,
                        name = L["Healer name"],
                        type = 'input',
                        get = function()
                          return self.healerName
                        end,
                        set = function(info, value)
                          self.healerName = value
                        end
                    },
                    newHealerButton = {
                        order = 2,
                        name = L["Add"],
                        type = 'execute',
                        func = function()
                          local name = self.healerName or ""
                          if not string.match(name, '%a') then
                            print(L["Invalid healer name"])
                            return
                          end

                          local trimmedName = strtrim(name)
                          local newHealer = {name = trimmedName}
                          table.insert(self.db.healers, newHealer)
        
                          self.healerName = nil
                          self:Register()
                        end
                    },
                    removeHealerButton = {
                        order = 3,
                        name = L["Remove"],
                        type = 'execute',
                        func = function()
                            if self.healerSelected then
                                table.remove(self.db.healers, self.healerSelected)
                                self.healerSelected = nil
                                self.healerName = nil
                                -- Impossible to redraw widget :-(
                            end
                        end
                    },
                    moveUpButton = {
                        order = 4,
                        name = L["Move up"],
                        type = 'execute',
                        func = function()
                            if not self.healerSelected then
                                return
                            end

                            local previousHealerIndex = ( self.healerSelected - 2 ) % #self.db.healers + 1
                            self.db.healers[self.healerSelected], self.db.healers[previousHealerIndex] = self.db.healers[previousHealerIndex], self.db.healers[self.healerSelected]
                            self.healerSelected = previousHealerIndex
                            -- Impossible to redraw widget :-(
                        end
                    },
                    moveDownButton = {
                        order = 5,
                        name = L["Move down"],
                        type = 'execute',
                        func = function()
                            if not self.healerSelected then
                                return
                            end

                            local nextHealerIndex = self.healerSelected % ( #self.db.healers ) + 1
                            self.db.healers[self.healerSelected], self.db.healers[nextHealerIndex] = self.db.healers[nextHealerIndex], self.db.healers[self.healerSelected]
                            self.healerSelected = nextHealerIndex
                            -- Impossible to redraw widget :-(
                        end
                    },
                }
            },
            announceGroup = {
                order = 4,
                type = 'group',
                inline = true,
                name = L["Announce"],
                args = {
                    outputChannel = {
                        order = 1,
                        type = 'select',
                        name = L["Channel"],
                        values = {
                            NONE = L["None"],
                            WINDOW = L["Chat window"],
                            SAY = L["Say"],
                            PARTY = L["Group"],
                            RAID = L["Raid"],
                            RAID_WARNING = L["Warning"]
                        },
                        sorting = {
                            'NONE',
                            'WINDOW',
                            'SAY',
                            'PARTY',
                            'RAID',
                            'RAID_WARNING'
                        },
                        get = function()
                            return self.db.outputChannel
                        end,
                        set = function(info, value)
                            self.db.outputChannel = value
                        end
                    },
                    testButton = {
                        order = 2,
                        type = 'execute',
                        name = L["Test"],
                        func = function()
                            self.addon:Test()
                        end
                    }
                }
            }
        }
    }
end

function HealOrderOptions:Register()
    AceConfig:RegisterOptionsTable(ADDON_NAME, self.content)
end

function HealOrderOptions:AddToBlizOptions()
    AceConfigDialog:AddToBlizOptions(ADDON_NAME, ADDON_NAME)
end

function HealOrderOptions:SetEnabled(info, value)
    self.db.enabled = value
end

function HealOrderOptions:GetEnabled()
    return self.db.enabled
end
