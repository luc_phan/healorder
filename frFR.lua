local L = LibStub("AceLocale-3.0"):NewLocale("HealOrder", "frFR")

L["Initializing..."] = "Initialisation..."
L["Enabling..."] = "Activation..."
L["Disabling..."] = "Désactivation..."

L["Enabled"] = "Activé"

L["Healers"] = "Soigneurs"
L["Name"] = "Nom"

L["Healer"] = "Soigneur"
L["Healer name"] = "Nom du soigneur"
L["Add"] = "Ajouter"
L["Invalid healer name"] = "Nom du soigneur incorrect"
L["Remove"] = "Supprimer"
L["Move up"] = "Monter"
L["Move down"] = "Descendre"

L["Announce"] = "Annonce"
L["Channel"] = "Canal"
L["None"] = "Aucun"
L["Chat window"] = "Fenêtre de discussion"
L["Say"] = "Dire"
L["Group"] = "Groupe"
L["Raid"] = "Raid"
L["Warning"] = "Avertissement"
L["Test"] = "Test"

L["Corrupted Mind"] = "Psyché corrompue"
L["Forbearance"] = "Longanimité"
L["Weakened Soul"] = "Ame affaiblie"

L["%s can still heal!"] = "%s peut encore soigner !"
