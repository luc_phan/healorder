-- Meta class

CarTable = {}

-- Base class method new

local sequence = 0

function CarTable:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self

   self.name = 'CarTable'
   self.columns = {
      {header='header1', caption="Header1", width=100},
      {header='header2', caption="Header2"},
      {header='header3', caption="Header3", width=300}
   }
   self.list = {
      {'cell1', 'cell2', 'cell3'},
      {'cell4', 'cell5', 'cell6'},
      {'cell7', 'cell8', 'cell9'},
   }
   self.defaultCellWidth = 200
   self.defaultCellHeight = 16
   self.rows = 5
   self.cells = nil
   self.selected = nil
   self.OnSelected = nil

   sequence = sequence + 1
   self.id = sequence

   return o
end

function CarTable:BuildFrame()
   local that = self

   local frame = CreateFrame('Frame')
   frame:SetPoint('CENTER')
   frame:SetWidth(self:GetFrameWidth())
   frame:SetHeight(self:GetFrameHeight())
   self.frame = frame

   self:BuildHeaders(frame)
   self:BuildCells(frame)

   local name = self.name .. 'ScrollFrame' .. self.id
   local scrollFrame = CreateFrame('ScrollFrame', name, frame, 'FauxScrollFrameTemplate')
   scrollFrame:SetWidth(self:GetScrollFrameWidth())
   scrollFrame:SetHeight(self:GetScrollFrameHeight())
   scrollFrame:SetPoint('TOPLEFT', 0, - self.defaultCellHeight)
   scrollFrame:SetScript(
      'OnVerticalScroll',
      function(self, offset)
         FauxScrollFrame_OnVerticalScroll(
            self,
            offset,
            that.defaultCellHeight,
            function(self)
               that:Update()
            end
         )
      end
   )
   scrollFrame:Show()

   FauxScrollFrame_Update(scrollFrame, #self.list, self.rows, self.defaultCellHeight)
   self.scrollFrame = scrollFrame

   return frame
end

function CarTable:BuildHeaders(frame)
   if not self.columns then
      return
   end

   local i, column, left
   left = 0
   for i, column in ipairs(self.columns) do
      local name = self.name .. 'Header' .. i
      local width = column.width or self.defaultCellWidth

      local button = CreateFrame("Button", name, frame)
      button:SetText(column.caption)
      button:SetPoint('TOPLEFT', left, 0)
      button:SetSize(width, self.defaultCellHeight)
      button:SetNormalFontObject("GameFontNormalLarge")

      local texture = button:CreateTexture()
      texture:SetTexture("Interface\\FriendsFrame\\WhoFrame-ColumnTabs")
      texture:SetTexCoord(0.25, 0.75, 0, 0.75)
      texture:SetAllPoints()
      button:SetNormalTexture(texture)

      left = left + width
   end
end

function CarTable:BuildCells(frame)
   self.cells = {}

   local j
   for j = 1, self.rows do
      local i, column, left
      left = 0
      for i, column in pairs(self.columns) do
         local name = self.name .. 'Cell' .. i .. '_' .. j
         local width = column.width or self.defaultCellWidth
         local button = CreateFrame("Button", name, frame)

         button:SetText(name)
         button:SetPoint('TOPLEFT', left, - j * self.defaultCellHeight)
         button:SetSize(width, self.defaultCellHeight)
         button:SetNormalFontObject("GameFontHighlightSmall")

         local texture = button:CreateTexture()
         texture:SetTexture("Interface\\QuestFrame\\UI-QuestTitleHighlight")
         texture:SetTexCoord(0.25, 0.75, 0, 0.75)
         texture:SetAllPoints()
         button:SetHighlightTexture(texture)
         button:SetScript(
            "OnClick",
            function(this, arg1)
               local offset = FauxScrollFrame_GetOffset(self.scrollFrame)
               local k = j + offset
               self:ToggleSelected(k)
               self:UpdateCells()
            end
         )

         if self.cells[j] == nil then
            self.cells[j] = {}
         end
         self.cells[j][i] = button

         left = left + width
      end
   end
end

function CarTable:SetList(list)
   self.list = list
end

function CarTable:GetFrameWidth()
   return self:GetScrollFrameWidth() + 32
end

function CarTable:GetFrameHeight()
   return self:GetScrollFrameHeight() + self.defaultCellHeight
end

function CarTable:GetScrollFrameWidth()
   if not self.columns then
      return 0
   end

   local width = 0
   for key, column in next, self.columns do
      width = width + (column.width or self.defaultCellWidth)
   end

   return width
end

function CarTable:GetScrollFrameHeight()
   return self.rows * self.defaultCellHeight
end

function CarTable:UpdateCells()
   local offset = FauxScrollFrame_GetOffset(self.scrollFrame)

   local j
   for j = 1, self.rows do
      local k = j + offset
      local i, column
      for i, column in pairs(self.columns) do
         local button = self.cells[j][i]
         if self.list[k] ~= nil then
            local row = self.list[k]
            local text = row[column.header]
            button:SetText(text)
            button:Show()
            if k == self:GetSelected() then
               button:LockHighlight()
            else
               button:UnlockHighlight()
            end
         else
            button:Hide()
         end
      end
   end
end

function CarTable:UpdateScrollFrame()
   FauxScrollFrame_Update(self.scrollFrame, #self.list, self.rows, self.defaultCellHeight)
end

function CarTable:Update()
   self:UpdateCells()
   self:UpdateScrollFrame()
end

function CarTable:SetSelected(index)
   self.selected = index
   if self.OnSelected then
      self:OnSelected(index)
   end
end

function CarTable:GetSelected()
   return self.selected
end

function CarTable:ToggleSelected(index)
   if self:GetSelected() == index then
      self:SetSelected(nil)
   else
      self:SetSelected(index)
   end
end
