-- #############
-- # Constants #
-- #############

local ADDON_NAME = "HealOrder"
local VERSION = "v" .. GetAddOnMetadata(ADDON_NAME, "Version") 

-- ###########
-- # Globals #
-- ###########

local Addon = LibStub("AceAddon-3.0"):NewAddon(ADDON_NAME, "AceConsole-3.0", "AceEvent-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale(ADDON_NAME)

-- ############
-- # Defaults #
-- ############

-- declare defaults to be used in the DB
local defaults = {
    realm = {
        enabled = true,
        healers = {{name="Urox"}, {name="Cuzco"}, {name="Luctrois"}}
    }
}

-- ##########
-- # Add-on #
-- ##########

function Addon:OnInitialize()
    -- Called when the addon is loaded
    self:Print(L["Initializing..."])

    self.addonName = ADDON_NAME
    self.version = VERSION
    self.PLAGUE = {
        29198, -- Shaman
        29194, -- Druid
        29196, -- Paladin
        29185, -- Priest
        -- 6788
    }
    self.PRIEST_CORRUPTED_MIND = 29184
    self.DRUID_CORRUPTED_MIND = 29195
    self.PALADIN_CORRUPTED_MIND = 29197
    self.SHAMAN_CORRUPTED_MIND = 29199
    self.CORRUPTED_MIND = {
        self.PRIEST_CORRUPTED_MIND,
        self.DRUID_CORRUPTED_MIND,
        self.PALADIN_CORRUPTED_MIND,
        self.SHAMAN_CORRUPTED_MIND,
        -- 6788
    }
    self.corruptedSpellName = "Corrupted Mind"
    -- self.corruptedSpellName = "Weakened Soul"
    self._raidStatus = nil
    self.nextHealer = nil
    self.encounterID = nil

    self.db = LibStub("AceDB-3.0"):New("HealOrderDB", defaults, true)

    self.options = HealOrderOptions:new({db = self.db.realm, addon = self})
    self.options:Register()
    self.options:AddToBlizOptions()

    self.mainFrame = MainFrame:new({addonName = self.addonName, version = self.version, variable = "_HealOrderMainFrame"})

    self:RegisterChatCommand("healorder", "ToggleWindow")  
    self:RegisterChatCommand("ho", "ToggleWindow")

    self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    self:RegisterEvent("ENCOUNTER_START")
    self:RegisterEvent("ENCOUNTER_END")
end

function Addon:OnEnable()
    -- Called when the addon is enabled
    self:Print(L["Enabling..."])
end
  
function Addon:OnDisable()
    -- Called when the addon is disabled
    self:Print(L["Disabling..."])
end

function Addon:ToggleWindow(input)
    if not self.mainFrame:IsVisible() then
        self.options:Register()
        self.mainFrame:Show()
    else
        self.mainFrame:Hide()
    end
end

function table.map(f, t)
    local index
    -- for index, value in ipairs(t) do -- ipairs() doesn't go over the nil values...
    for index = 1, #t do
        value = t[index]
        if value == nil then
            t[index] = 'nil'
        else
            t[index] = f(value)
        end
    end
end
  
function Addon:COMBAT_LOG_EVENT_UNFILTERED()
    -- process the event

    if (not self.db.realm.enabled) or (self.encounterID ~= 1115) then
        return
    end

    local info = {CombatLogGetCurrentEventInfo()}
    local event = info[2]

    if event == "SPELL_AURA_APPLIED" then

        local healer = info[5]
        local target = info[9]
        local spell = info[13]
        local amount = info[16]

        if spell ~= L[self.corruptedSpellName] then
            return
        end

        local healerIndex = self:GetHealerIndex(healer)
        if not healerIndex then
            return
        end

        self:UpdateRaidStatus()

        if not self:Corrupted(healer) then
            return
        end

        self:NotifyNextHealer(healerIndex, healer)

    elseif event == "UNIT_DIED" then

        local healer = info[9]

        if not self.nextHealer or strlower(healer) ~= strlower(self.nextHealer) then
            return
        end

        local healerIndex = self:GetHealerIndex(healer)
        if not healerIndex then
            return
        end

        self:UpdateRaidStatus()
        self:NotifyNextHealer(healerIndex, healer)

    elseif event == "SPELL_HEAL" then

        local healer = info[5]

        if not self.nextHealer or strlower(healer) ~= strlower(self.nextHealer) then
            return
        end

        -- C_Timer.After(1, function() self:AfterHeal(healer) end)

        local healerIndex = self:GetHealerIndex(healer)
        self:Print(healerIndex)
        if not healerIndex then
            return
        end

        self:UpdateRaidStatus()

        if not self:hasPlague(healer) then
            C_Timer.After(1, function() self:AfterHeal(healer) end)
        end

    end
end

function Addon:ENCOUNTER_START(event, encounterID, encounterName, difficultyID, groupSize)
    self.encounterID = encounterID
    self.nextHealer = self.db.realm.healers[1]
end

function Addon:ENCOUNTER_END(event, encounterID, encounterName, difficultyID, groupSize, success)
    self.encounterID = nil
end

function Addon:NotifyNextHealer(healerIndex, healer)
    local nextHealer = self:GetNextHealerByIndex(healerIndex)
    if not nextHealer then
        nextHealer = self:GetNextHealerByDebuff()
    end

    self.nextHealer = nextHealer

    local remainingTime
    if nextHealer then
        remainingTime = self._raidStatus[strlower(nextHealer)].remainingTime
    end

    local message
    if remainingTime then
        message = string.format("%d = %s >>> %s (%d)", healerIndex, healer, nextHealer, remainingTime)
    elseif nextHealer then
        message = string.format("%d = %s >>> %s", healerIndex, healer, nextHealer)
    else
        message = string.format("%d = %s >>> ???", healerIndex, healer)
    end
    self:SendMessage(message, self.db.realm.outputChannel)
end

function Addon:GetHealerIndex(name)
    if not name then
        return
    end

    local i, healer
    for i, healer in pairs(self.db.realm.healers) do
        if strlower(healer.name) == strlower(name) then
            return i
        end
    end
end

function Addon:GetNextHealer(index)
    local nextIndex = index
    while true do
        nextIndex = nextIndex % #self.db.realm.healers + 1
        local healer = self.db.realm.healers[nextIndex].name
        if self:IsAvailable(healer) or nextIndex == index then
            return healer
        end
    end
end

function Addon:GetNextHealerByIndex(index)
    local nextIndex = index
    while true do
        nextIndex = nextIndex % #self.db.realm.healers + 1
        local healer = self.db.realm.healers[nextIndex].name
        if self:IsAvailable(healer) and not self:Corrupted(healer) then
            return healer
        end
        if nextIndex == index then
            return
        end
    end
end

function Addon:IsAvailable(healer)
    return not self:IsDeadOrGhost(healer) and self:IsOnline(healer)
end

function Addon:SendMessage(message, channel)
    if not channel or channel == 'NONE' then
        return
    elseif channel == 'WINDOW' then
        self:Print(message)
    else
        SendChatMessage(message, channel)
    end
end

function Addon:GetRaidStatus()
    local status = {}

    if IsInRaid() then
        local i, j
        for i = 1, 40 do
            local unitId = "raid" .. i

            local player = UnitName(unitId)
            if not player then
                break
            end
            player = strlower(player)

            status[player] = self:GetUnitStatus(unitId)
        end
    else
        local unitId = "player"
        local player = UnitName(unitId)
        player = strlower(player)
        status[player] = self:GetUnitStatus(unitId)
    end

    return status
end

function Addon:GetUnitStatus(unitId)
    local status = {
        debuffs = {},
        remainingTime = nil,
        isDeadOrGhost = nil,
        isOnline = nil
    }

    for j = 1, 32 do
        local name, _, _, _, duration, expirationTime, _, _, _, spellId  = UnitDebuff(unitId, j)

        if not name then
            break
        end

        local remainingTime = expirationTime - GetTime()
        status.debuffs[spellId] = {name=name, remainingTime=remainingTime}

        for _, corrupted in pairs(self.CORRUPTED_MIND) do
            if spellId == corrupted then
                status.remainingTime = remainingTime
            end
        end
    end

    status.isDeadOrGhost = UnitIsDeadOrGhost(unitId)
    status.isOnline = UnitIsConnected(unitId)

    return status
end

function Addon:GetNextHealerByDebuff()
    local i, healer, lowestTime, nextHealer

    for i, healer in pairs(self.db.realm.healers) do
        local name = strlower(healer.name)
        if self:IsAvailable(name) and self._raidStatus[name] and self._raidStatus[name].debuffs then
            local spellId, spellStatus, corrupted
            for spellId, spellStatus in pairs(self._raidStatus[name].debuffs) do
                for _, corrupted in pairs(self.CORRUPTED_MIND) do
                    if spellId == corrupted then
                        if not lowestTime or spellStatus.remainingTime < lowestTime then
                            lowestTime = spellStatus.remainingTime
                            nextHealer = healer.name
                        end
                    end
                end
            end
        end
    end

    return nextHealer
end

function Addon:UpdateRaidStatus()
    self._raidStatus = self:GetRaidStatus()
end

function Addon:Corrupted(healer)
    healer = strlower(healer)

    if not self._raidStatus[healer] or not self._raidStatus[healer].debuffs then
        return
    end

    local spellId, spellStatus, corrupted
    for spellId, spellStatus in pairs(self._raidStatus[healer].debuffs) do
        for _, corrupted in pairs(self.CORRUPTED_MIND) do
            if spellId == corrupted then
                return true
            end
        end
    end

    return false
end

function Addon:hasPlague(healer)
    healer = strlower(healer)

    if not self._raidStatus[healer] or not self._raidStatus[healer].debuffs then
        return
    end

    local spellId, spellStatus, plague
    for spellId, spellStatus in pairs(self._raidStatus[healer].debuffs) do
        for _, plague in pairs(self.PLAGUE) do
            if spellId == plague then
                return true
            end
        end
    end

    return false
end

function Addon:IsDeadOrGhost(player)
    player = strlower(player)
    if not self._raidStatus[player] then
        return nil
    end
    return self._raidStatus[player].isDeadOrGhost
end

function Addon:IsOnline(player)
    player = strlower(player)
    if not self._raidStatus[player] then
        return nil
    end
    return self._raidStatus[player].isOnline
end

function Addon:Test()
    local healer = self.nextHealer

    local healerIndex = self:GetHealerIndex(healer)
    if not healerIndex then
        healerIndex = 1
        if not self.db.realm.healers[healerIndex] then
            self:Print("Healer list empty")
            return
        end
        healer = self.db.realm.healers[healerIndex].name
    end

    self:UpdateRaidStatus()
    self:NotifyNextHealer(healerIndex, healer)
end

function Addon:AfterHeal(healer)
    self:UpdateRaidStatus()
    if not self:Corrupted(healer) then
        local message = string.format(L["%s can still heal!"], healer)
        self:SendMessage(message, self.db.realm.outputChannel)
    end
end
