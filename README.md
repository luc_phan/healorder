Heal Order
==========

Description
-----------

**HealOrder** is a **WoW Classic** add-on to announce which healer is corrupted and which one should be next in **Loatheb** fight in **Naxxramas**.

Usage
-----

- Type `/healorder` or `/ho` to open the main window.
- Create a list of healers.
    + The interface is a bit buggy.
- Set an output channel.

Ideas
-----

- Use `lib-ScrollingTable` next time
- Stop using AceConfig... Too much issues, impossible to redraw widget...
- ~~Send message if next healer dies.~~

Changes
-------

- 2021-03-25 (0.4.1) = fix: warn early heal if no plague
- 2021-03-18 (0.4.0) = feat: warn on early heal
- 2021-03-11 (0.3.0) = improve: skip if next healer is corrupted
- 2021-03-06 (0.2.3) = fix: case insensitive in death event
- 2021-02-04 (0.2.2) = fix: case insensitive for healers' name
- 2021-01-14 (0.2.1) = improve: skip if encounter is not loatheb
- 2020-12-23 (0.2.0) = feat: add test button
- 2020-12-22 (0.1.0) = feat: notify if next healer dies
- 2020-12-22 (0.0.4) = fix: trim healer name on add
- 2020-12-18 (0.0.3) = feat: skip healer if unavailable
- 2020-12-17 (0.0.2) = fix: detect corrupted mind instead of heal
